﻿using System.Collections.Generic;
using PatientOrganizer.Model;

namespace PatientOrganizer.UI.Data
{
    public class PatientDataService : IPatientDataService
    {
        public IEnumerable<Patient> GetAll()
        {
            yield return new Patient {FirstName = "John", LastName = "Dow"};
            yield return new Patient {FirstName = "Jane", LastName = "Smith"};
            yield return new Patient {FirstName = "Mable", LastName = "Fame"};
            yield return new Patient {FirstName = "Olivia", LastName = "Scott"};
        }
    }
}
