﻿using System.Collections.ObjectModel;
using PatientOrganizer.Model;
using PatientOrganizer.UI.Data;

namespace PatientOrganizer.UI.ViewModel
{
    class MainViewModel
    {
        private IPatientDataService _patientDataService;
        private Patient _selectedPatient;
        public MainViewModel(IPatientDataService patientDataService)
        {
            Patients = new ObservableCollection<Patient>();
            _patientDataService = patientDataService;
        }

        public void Load()
        {
            var patients = _patientDataService.GetAll();
            Patients.Clear();
            foreach (var patient in patients)
            {
                Patients.Add(patient);
            }
        }
        public ObservableCollection<Patient> Patients { get; set; }

        public Patient SelectedPatient
        {
            get { return _selectedPatient; }
            set { _selectedPatient = value; }
        }

    }
}
