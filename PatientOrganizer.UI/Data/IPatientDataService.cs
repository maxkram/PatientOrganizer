﻿using System.Collections.Generic;
using PatientOrganizer.Model;

namespace PatientOrganizer.UI.Data
{
    interface IPatientDataService
    {
        IEnumerable<Patient> GetAll();
    }
}
